package fr.iut.pm.movieapplication.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fr.iut.pm.movieapplication.model.media.MediaResult
import fr.iut.pm.movieapplication.repository.api.MediaRepository
import kotlinx.coroutines.launch

class HomeSectionsVM : ViewModel() {

    private val repository = MediaRepository()

    private var _trendsLiveData : MutableLiveData<List<MediaResult>> = MutableLiveData()
    fun getTrendsLiveData() : MutableLiveData<List<MediaResult>> = _trendsLiveData

    private var _popularMoviesLiveData : MutableLiveData<List<MediaResult>> = MutableLiveData()
    fun getPopularMoviesLiveData() : MutableLiveData<List<MediaResult>> = _popularMoviesLiveData

    private var _popularTvShowsLiveData : MutableLiveData<List<MediaResult>> = MutableLiveData()
    fun getPopularTvShowsLiveData() : MutableLiveData<List<MediaResult>> = _popularTvShowsLiveData


    init {
        viewModelScope.launch {
            _trendsLiveData.postValue(repository.getTrends())
            _popularMoviesLiveData.postValue(repository.getPopularMovies())
            _popularTvShowsLiveData.postValue(repository.getPopularTvShows())
        }
    }


}