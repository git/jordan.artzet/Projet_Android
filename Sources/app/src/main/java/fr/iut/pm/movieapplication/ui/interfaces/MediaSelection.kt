package fr.iut.pm.movieapplication.ui.interfaces

interface MediaSelection {

    fun onMediaSelected(mediaId : Int)
}