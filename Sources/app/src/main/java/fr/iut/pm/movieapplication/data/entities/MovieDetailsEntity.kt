package fr.iut.pm.movieapplication.data.entities

import androidx.room.*
import fr.iut.pm.movieapplication.model.Genre


@Entity(tableName = "movies_details_table")
data class MovieDetailsEntity(
    @PrimaryKey @ColumnInfo("movie_id") val movieId : Int,
    @Embedded val movie : MovieEntity,
    val budget : Int,
    val homepage : String?,
    val revenue : Long,
    val status : String
    //Future attributes to add
) {


}