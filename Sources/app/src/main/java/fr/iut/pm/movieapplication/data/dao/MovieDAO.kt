package fr.iut.pm.movieapplication.data.dao

import androidx.room.*
import fr.iut.pm.movieapplication.data.entities.*
import fr.iut.pm.movieapplication.model.media.movie.MovieDetails
import kotlinx.coroutines.flow.Flow

@Dao
interface MovieDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(movieEntity : MovieEntity)

    /**
     * Get all movies from the movies_table
     */
    @Query("SELECT * FROM movies_table")
    suspend fun getAllMovies() : List<MovieEntity>

    /**
     * Get all movies from the movies_details_table
     */
    @Query("SELECT * FROM movies_details_table")
    suspend fun getAllMoviesDetails() : List<MovieDetailsEntity>

    /**
     * Get a movie by id
     */
    @Query("SELECT * FROM movies_table WHERE id = :id")
    fun getMovieById(id : Int) : MovieEntity

    //delete a movie
    @Query("DELETE FROM movies_table WHERE id = :id")
    suspend fun deleteMovieById(id : Int)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMovieDetails(movieDetailsEntity: MovieDetailsEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertGenres(genres : List<GenreEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMovieDetailsGenres(movieDetailsGenresEntity : List<MovieDetailsGenreEntity>)

    @Transaction
    suspend fun insertMovieDetailsWithGenres(movieDetailsWithGenres : MovieDetailsWithGenres) {
        insert(movieDetailsWithGenres.movieDetails.movie)
        insertMovieDetails(movieDetailsWithGenres.movieDetails)
        insertGenres(movieDetailsWithGenres.genres)
        val movieDetailsGenres = movieDetailsWithGenres.genres.map {
            MovieDetailsGenreEntity(movieDetailsWithGenres.movieDetails.movieId, it.genreId)
        }
        insertMovieDetailsGenres(movieDetailsGenres)
    }

    @Transaction
    @Query("SELECT * FROM movies_details_table WHERE movie_id = :movieId")
    suspend fun getMovieDetailsWithGenresById(movieId: Int): MovieDetailsWithGenres


}