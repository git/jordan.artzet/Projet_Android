package fr.iut.pm.movieapplication.model.media

data class MediaResult(

    val posterPath: String? = null,
    val adult: Boolean?,
    val overview: String,
    val releaseDate: String,
    val originCountry: List<String>? = null,
//    val genreIds: List<Int>,
    val id: Int,
    val originalTitle: String,
    val originalLanguage: String,
    val title: String,
    val backdropPath: String? = null,
    val popularity: Double,
    val voteCount: Int,
    val voteAverage: Double,
    val mediaType: String?
)
{}