package fr.iut.pm.movieapplication.repository.local

import android.util.Log
import fr.iut.pm.movieapplication.data.dao.MovieDAO
import fr.iut.pm.movieapplication.data.mapper.MovieLocalMapper
import fr.iut.pm.movieapplication.model.media.movie.Movie
import fr.iut.pm.movieapplication.model.media.movie.MovieDetails
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MovieLocalRepository(private val movieDAO: MovieDAO) {

    suspend fun insertMovie(movie : Movie) = withContext(Dispatchers.IO) {
        movieDAO.insert(MovieLocalMapper.mapToMovieEntity(movie))
    }

    suspend fun insertMovieDetails(movieDetails : MovieDetails) = withContext(Dispatchers.IO) {
        Log.i("MovieLocalRepository", "INSERT MOVIE DETAILS")
        movieDAO.insertMovieDetailsWithGenres(MovieLocalMapper.mapToMovieDetailsWithGenres(movieDetails))
    }

    suspend fun getMovieDetailsById(id : Int) : MovieDetails {
        return MovieLocalMapper.mapToMovieDetails(movieDAO.getMovieDetailsWithGenresById(id))
    }

    suspend fun getAllMovies() : List<Movie> = withContext(Dispatchers.IO) {
        val listMovie : MutableList<Movie> = mutableListOf()
        val listMoviesEntities = movieDAO.getAllMovies()

        listMoviesEntities.forEach { movieEntity ->
            listMovie.add(MovieLocalMapper.mapToMovie(movieEntity))
        }

        listMovie
    }
}