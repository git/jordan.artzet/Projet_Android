package fr.iut.pm.movieapplication.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import fr.iut.pm.movieapplication.databinding.FragmentSearchResultsBinding
import fr.iut.pm.movieapplication.ui.adapter.MediaAdapter
import fr.iut.pm.movieapplication.ui.adapter.SearchResultAdapter
import fr.iut.pm.movieapplication.ui.interfaces.MediaSelection
import fr.iut.pm.movieapplication.ui.viewmodel.SearchResultVM
import fr.iut.pm.movieapplication.ui.viewmodel.SearchResultVMFactory

class SearchResultFragment : Fragment(), MediaSelection {

    private val searchResultViewModel by viewModels<SearchResultVM> { SearchResultVMFactory(arguments?.getString("query")!!) }
    private val searchResultAdapter = SearchResultAdapter(this)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentSearchResultsBinding.inflate(inflater, container, false)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.searchResultsVM = searchResultViewModel

        with(binding.searchResultsItemRecyclerView) {
            this.adapter = searchResultAdapter
        }

        return binding.root;
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        searchResultViewModel.getSearchResultLiveData().observe(viewLifecycleOwner) {
            searchResultAdapter.submitList(it)
        }

    }

    override fun onMediaSelected(mediaId: Int) {
        Log.d("ITEM SELECTED", mediaId.toString())
    }

    companion object {
        fun newInstance(query: String): SearchResultFragment {
            val fragment = SearchResultFragment()
            val args = Bundle()
            args.putString("query", query)
            fragment.arguments = args
            return fragment
        }
    }
}