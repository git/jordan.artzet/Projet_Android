package fr.iut.pm.movieapplication.model

import androidx.room.ColumnInfo
import androidx.room.Embedded
import fr.iut.pm.movieapplication.model.media.movie.MovieDetails

data class Popular(
    @ColumnInfo("page")
    val page : Int,
    @ColumnInfo("results")
    @Embedded
    val results : List<MovieDetails>,
    val totalResults : Int,
    val totalPages : Int

){

}