package fr.iut.pm.movieapplication.ui.viewmodel

import androidx.lifecycle.*
import fr.iut.pm.movieapplication.data.database.MovieDataBase
import fr.iut.pm.movieapplication.model.media.movie.MovieDetails
import fr.iut.pm.movieapplication.repository.api.MovieAPIRepository
import fr.iut.pm.movieapplication.repository.local.MovieLocalRepository
import kotlinx.coroutines.launch

class MoviesDialogVM(private val movieId : Int) : ViewModel() {

    private val repository = MovieAPIRepository()

    private val movieLocalRepository = MovieLocalRepository(
        MovieDataBase.getInstance().movieDAO())

    private var _movieDetailsLiveData : MutableLiveData<MovieDetails> = MutableLiveData()
    fun getMovieDetailLiveData() : LiveData<MovieDetails> = _movieDetailsLiveData

    init {
        viewModelScope.launch {
            if(movieId != 0) _movieDetailsLiveData.postValue(repository.getMovieDetails(movieId))
        }
    }

    fun register(movieDetails: MovieDetails) = viewModelScope.launch {

        movieLocalRepository.insertMovieDetails(movieDetails)
    }

}

class MoviesDialogVMFactory(private val movieId : Int) : ViewModelProvider.Factory
{
    override fun<T:ViewModel>create(modelClass:Class<T>) : T
    {
        return MoviesDialogVM(movieId) as T
    }
}