package fr.iut.pm.movieapplication.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import fr.iut.pm.movieapplication.R
import fr.iut.pm.movieapplication.ui.fragments.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loadFragments(HomeSectionsFragment())

        val navigationView = findViewById<BottomNavigationView>(R.id.navigation_view)
        navigationView.setOnItemSelectedListener {
            when(it.itemId)
            {
                R.id.home_page -> {
                    loadFragments(HomeSectionsFragment())
                    return@setOnItemSelectedListener true
                }

                R.id.movies_page -> {
                    loadFragments(MoviesFragment())
                    return@setOnItemSelectedListener true
                }

                R.id.series_page -> {
                    loadFragments(TvShowsFragment())
                    return@setOnItemSelectedListener true
                }
                R.id.favorites_page -> {
                    loadFragments(FavoritesFragment())
                    return@setOnItemSelectedListener true
                }
                else -> false
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.app_menu, menu)

        val searchItem = menu?.findItem(R.id.search_bar)
        val searchView : SearchView = searchItem?.actionView as SearchView

        searchView.setOnQueryTextListener( object : SearchView.OnQueryTextListener {

            override fun onQueryTextSubmit(query: String?): Boolean {
                if(!query.isNullOrEmpty()) {
                    loadFragments(SearchResultFragment.newInstance(query))
                }
                return true;
            }
            //A améliorer
            override fun onQueryTextChange(query: String?): Boolean {
                if(!query.isNullOrEmpty()) {
                    loadFragments(SearchResultFragment.newInstance(query))
                }
                return true
            }

        })

        return true
    }

    private fun loadFragments(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}