package fr.iut.pm.movieapplication.utils

import fr.iut.pm.movieapplication.api.dtos.GenreDTO
import fr.iut.pm.movieapplication.api.dtos.MediaResultDTO
import fr.iut.pm.movieapplication.api.dtos.MovieDetailsDTO
import fr.iut.pm.movieapplication.model.Genre
import fr.iut.pm.movieapplication.model.media.MediaResult
import fr.iut.pm.movieapplication.model.media.movie.Movie
import fr.iut.pm.movieapplication.model.media.movie.MovieDetails
import fr.iut.pm.movieapplication.model.media.tvshow.TvShow

object MediaResultMapper {

     fun mapToTvShow(mediaResultDTO: MediaResultDTO): TvShow {
        return TvShow(
            posterPath = mediaResultDTO.posterPath,
            popularity = mediaResultDTO.popularity!!,
            id = mediaResultDTO.id,
            backdropPath = mediaResultDTO.backdropPath ?: "",
            voteAverage = mediaResultDTO.voteAverage!!,
            overview = mediaResultDTO.overview ?: "",
            firstAirDate = mediaResultDTO.firstAirDate,
            originCountry = mediaResultDTO.originCountry!!,
//            genreIds = mediaResultDTO.genreIds,
            originalLanguage = mediaResultDTO.originalLanguage!!,
            voteCount = mediaResultDTO.voteCount!!,
            name = mediaResultDTO.name!!,
            originalName = mediaResultDTO.originalName!!
        )
    }

     fun mapToMovie(mediaResultDTO: MediaResultDTO): Movie {
        return Movie(
            posterPath = mediaResultDTO.posterPath,
            adult = mediaResultDTO.adult,
            overview = mediaResultDTO.overview ?: "",
            releaseDate = mediaResultDTO.releaseDate ?: "",
//            genreIds = mediaResultDTO.genreIds,
            id = mediaResultDTO.id,
            originalTitle = mediaResultDTO.originalTitle!!,
            originalLanguage = mediaResultDTO.originalLanguage!!,
            title = mediaResultDTO.title!!,
            backdropPath = mediaResultDTO.backdropPath,
            popularity = mediaResultDTO.popularity!!,
            voteCount = mediaResultDTO.voteCount!!,
            //video = mediaResultDTO.video,
            voteAverage = mediaResultDTO.voteAverage!!
        )
    }

    fun mapToMediaResult(mediaResultDTO: MediaResultDTO) : MediaResult {
        return MediaResult(
            posterPath = mediaResultDTO.posterPath,
            adult = mediaResultDTO.adult,
            overview = mediaResultDTO.overview ?: "",
            releaseDate = mediaResultDTO.releaseDate ?: mediaResultDTO.firstAirDate!! ,
            originCountry = mediaResultDTO.originCountry,
//            genreIds = mediaResultDTO.genreIds,
            id = mediaResultDTO.id,
            originalTitle = mediaResultDTO.originalTitle ?: mediaResultDTO.originalName!!, //if it's not a movie also it's a tvshow
            originalLanguage = mediaResultDTO.originalLanguage!!,
            title = mediaResultDTO.title ?: mediaResultDTO.name!!, //if it's not a movie also it's a tvshow
            backdropPath = mediaResultDTO.backdropPath,
            popularity = mediaResultDTO.popularity!!,
            voteCount = mediaResultDTO.voteCount!!,
            voteAverage = mediaResultDTO.voteAverage!!,
            mediaType = mediaResultDTO.mediaType
        )
    }

    fun mapToMovieDetails(movieDetailsDTO: MovieDetailsDTO?): MovieDetails? {
        if(movieDetailsDTO == null) return null
        return MovieDetails(
            posterPath = movieDetailsDTO.posterPath,
            adult = movieDetailsDTO.adult!!,
            overview = movieDetailsDTO.overview ?: "",
            releaseDate = movieDetailsDTO.releaseDate ?: "",
            id = movieDetailsDTO.id,
            originalTitle = movieDetailsDTO.originalTitle!!,
            originalLanguage = movieDetailsDTO.originalLanguage,
            title = movieDetailsDTO.title,
            backdropPath = movieDetailsDTO.backdropPath,
            popularity = movieDetailsDTO.popularity,
            voteCount = movieDetailsDTO.voteCount,
            voteAverage = movieDetailsDTO.voteAverage,
            budget = movieDetailsDTO.budget,
            genres = movieDetailsDTO.genres.map { mapGenreDTOToGenre(it) },
            homepage = movieDetailsDTO.homepage,
            revenue = movieDetailsDTO.revenue,
            status = movieDetailsDTO.status
        )

    }

    fun mapGenreDTOToGenre(genreDTO : GenreDTO) : Genre {
        return Genre(
            name = genreDTO.name,
            id = genreDTO.id
        )
    }


}