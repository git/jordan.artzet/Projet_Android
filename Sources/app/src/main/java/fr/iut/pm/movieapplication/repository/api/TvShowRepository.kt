package fr.iut.pm.movieapplication.repository.api

import android.util.Log
import fr.iut.pm.movieapplication.api.RetrofitInstance
import fr.iut.pm.movieapplication.model.media.tvshow.TvShow
import fr.iut.pm.movieapplication.utils.MediaResultMapper

class TvShowRepository {

    suspend fun getPopularTvShows(page : Int = 1 ) : List<TvShow> {

        val listMovie : MutableList<TvShow> = mutableListOf()

        val response = RetrofitInstance.api.getPopularTvShows(page = page)
        if (response.isSuccessful) {
            Log.d("List :", response.body().toString())
            val popularDTO = response.body()
            val listMoviesDTO = popularDTO?.results
            listMoviesDTO?.forEach {
                val tvShow = MediaResultMapper.mapToTvShow(it)
                listMovie.add(tvShow)
                Log.d("Movie ", tvShow.name )
            }
        }
        else Log.d("Error failure", response.message())

        return listMovie
    }

    suspend fun getAiringTodayTvShows(page : Int = 1 ) : List<TvShow> {

        val listMovie : MutableList<TvShow> = mutableListOf()

        val response = RetrofitInstance.api.getAiringTodayTvShows(page = page)
        if (response.isSuccessful) {
            Log.d("List :", response.body().toString())
            val popularDTO = response.body()
            val listMoviesDTO = popularDTO?.results
            listMoviesDTO?.forEach {
                val tvShow = MediaResultMapper.mapToTvShow(it)
                listMovie.add(tvShow)
                Log.d("Movie ", tvShow.name )
            }
        }
        else Log.d("Error failure", response.message())

        return listMovie
    }

    suspend fun getTvOnTheAirTvShows(page : Int = 1 ) : List<TvShow> {

        val listMovie : MutableList<TvShow> = mutableListOf()

        val response = RetrofitInstance.api.getTvOnTheAirTvShows(page = page)
        if (response.isSuccessful) {
            Log.d("List :", response.body().toString())
            val popularDTO = response.body()
            val listMoviesDTO = popularDTO?.results
            listMoviesDTO?.forEach {
                val tvShow = MediaResultMapper.mapToTvShow(it)
                listMovie.add(tvShow)
                Log.d("Movie ", tvShow.name )
            }
        }
        else Log.d("Error failure", response.message())

        return listMovie
    }

    suspend fun getTopRatedTvShows(page : Int = 1 ) : List<TvShow> {

        val listMovie : MutableList<TvShow> = mutableListOf()

        val response = RetrofitInstance.api.getTopRatedTvShows(page = page)
        if (response.isSuccessful) {
            Log.d("List :", response.body().toString())
            val popularDTO = response.body()
            val listMoviesDTO = popularDTO?.results
            listMoviesDTO?.forEach {
                val tvShow = MediaResultMapper.mapToTvShow(it)
                listMovie.add(tvShow)
                Log.d("Movie ", tvShow.name )
            }
        }
        else Log.d("Error failure", response.message())

        return listMovie
    }
}