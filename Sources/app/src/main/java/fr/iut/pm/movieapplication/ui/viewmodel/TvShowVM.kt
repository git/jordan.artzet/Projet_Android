package fr.iut.pm.movieapplication.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fr.iut.pm.movieapplication.model.media.tvshow.TvShow
import fr.iut.pm.movieapplication.repository.api.TvShowRepository
import fr.iut.pm.movieapplication.ui.adapter.TvShowAdapter
import kotlinx.coroutines.launch

class TvShowVM : ViewModel() {

    private val tvShowRepository = TvShowRepository()

    private var tvShowLiveData : MutableLiveData<List<TvShow>> = MutableLiveData<List<TvShow>>()
    fun getTvShowLiveData() : LiveData<List<TvShow>> = tvShowLiveData

    private var currentFilter : String = ""
    val tvShowAdapter = TvShowAdapter()

    private var currentPage = 1

    fun getData(filter : String) {

        currentFilter = filter
        currentPage = 1
        when(currentFilter) {
            "Populaires" -> viewModelScope.launch {
                tvShowLiveData.postValue(tvShowRepository.getPopularTvShows())
            }
            "Diffusées aujourd\'hui" -> viewModelScope.launch {
                tvShowLiveData.postValue(tvShowRepository.getAiringTodayTvShows())
            }
            "En cours de diffusion" -> viewModelScope.launch {
                tvShowLiveData.postValue(tvShowRepository.getTvOnTheAirTvShows())
            }
            "Les mieux notées" -> viewModelScope.launch {
                tvShowLiveData.postValue(tvShowRepository.getTopRatedTvShows())
            }
        }
    }
}