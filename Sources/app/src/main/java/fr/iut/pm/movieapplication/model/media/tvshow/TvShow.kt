package fr.iut.pm.movieapplication.model.media.tvshow

open class TvShow(
    open val posterPath: String?,
    open val popularity: Double,
    open val id: Int,
    open val backdropPath: String,
    open val voteAverage: Double,
    open val overview: String,
    open val firstAirDate: String?,
    open val originCountry: List<String>,
//    open val genreIds: List<Int>,
    open val originalLanguage: String,
    open val voteCount: Int,
    open val name: String,
    open val originalName: String,
    open val mediaType: String = "tv",
)
{

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TvShow

        if (posterPath != other.posterPath) return false
        if (popularity != other.popularity) return false
        if (id != other.id) return false
        if (backdropPath != other.backdropPath) return false
        if (voteAverage != other.voteAverage) return false
        if (overview != other.overview) return false
        if (firstAirDate != other.firstAirDate) return false
        if (originCountry != other.originCountry) return false
//        if (genreIds != other.genreIds) return false
        if (originalLanguage != other.originalLanguage) return false
        if (voteCount != other.voteCount) return false
        if (name != other.name) return false
        if (originalName != other.originalName) return false
        if (mediaType != other.mediaType) return false

        return true
    }

    override fun hashCode(): Int {
        var result = posterPath?.hashCode() ?: 0
        result = 31 * result + popularity.hashCode()
        result = 31 * result + id
        result = 31 * result + backdropPath.hashCode()
        result = 31 * result + voteAverage.hashCode()
        result = 31 * result + overview.hashCode()
        result = 31 * result + (firstAirDate?.hashCode() ?: 0)
        result = 31 * result + originCountry.hashCode()
//        result = 31 * result + genreIds.hashCode()
        result = 31 * result + originalLanguage.hashCode()
        result = 31 * result + voteCount
        result = 31 * result + name.hashCode()
        result = 31 * result + originalName.hashCode()
        result = 31 * result + mediaType.hashCode()
        return result
    }
}