package fr.iut.pm.movieapplication.utils

import fr.iut.pm.movieapplication.api.dtos.MovieDTO
import fr.iut.pm.movieapplication.api.dtos.MovieDetailsDTO
import fr.iut.pm.movieapplication.model.media.movie.Movie
import fr.iut.pm.movieapplication.model.media.movie.MovieDetails

object MovieMapper {

    fun mapToMovie(movieDTO : MovieDTO) : Movie {
        return Movie(
            posterPath = movieDTO.posterPath,
            adult = movieDTO.adult,
            overview = movieDTO.overview,
            releaseDate = movieDTO.releaseDate,
            id = movieDTO.id,
            originalTitle = movieDTO.originalTitle,
            originalLanguage = movieDTO.originalLanguage,
            title = movieDTO.title,
            backdropPath = movieDTO.backdropPath,
            popularity = movieDTO.popularity,
            voteCount = movieDTO.voteCount,
            voteAverage = movieDTO.voteAverage
        )
    }

    fun mapToMovieDetails(movieDetailsDTO: MovieDetailsDTO ) : MovieDetails {
        return MovieDetails(
            posterPath = movieDetailsDTO.posterPath,
            adult = movieDetailsDTO.adult!!,
            overview = movieDetailsDTO.overview ?: "",
            releaseDate = movieDetailsDTO.releaseDate ?: "",
            id = movieDetailsDTO.id,
            originalTitle = movieDetailsDTO.originalTitle,
            originalLanguage = movieDetailsDTO.originalLanguage,
            title = movieDetailsDTO.title,
            backdropPath = movieDetailsDTO.backdropPath,
            popularity = movieDetailsDTO.popularity,
            voteCount = movieDetailsDTO.voteCount,
            voteAverage = movieDetailsDTO.voteAverage,
            budget = movieDetailsDTO.budget,
            genres = movieDetailsDTO.genres.map { MediaResultMapper.mapGenreDTOToGenre(it) },
            homepage = movieDetailsDTO.homepage,
            revenue = movieDetailsDTO.revenue,
            status = movieDetailsDTO.status
        )
    }
}