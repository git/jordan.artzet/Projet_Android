package fr.iut.pm.movieapplication.api.dtos

import com.squareup.moshi.Json

open class TvShowDetailsDTO(

    @Json(name = "poster_path")
    open val posterPath: String?,
    open val popularity: Double,
    open val id: Int,
    @Json(name = "backdrop_path")
    open val backdropPath: String?,
    @Json(name = "vote_average")
    open val voteAverage: Double,
    open val overview: String,
    @Json(name = "first_air_date")
    open val firstAirDate: String,
    @Json(name = "origin_country")
    open val originCountry: List<String>,
    @Json(name = "genre_ids")
    open val genreIds: List<Int>,
    @Json(name = "original_language")
    open val originalLanguage: String,
    @Json(name = "vote_count")
    open val voteCount: Int,
    open val name: String,
    @Json(name = "original_name")
    open val originalName: String,
    )
{}