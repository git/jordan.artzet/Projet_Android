package fr.iut.pm.movieapplication.api.dtos


import com.squareup.moshi.Json



class ResultDTO(
    @Json(name = "page")
    val page : Int,
    @Json(name = "results")
    val results : List<MediaResultDTO>,
    @Json(name = "total_results")
    val totalResults : Int,
    @Json(name = "total_pages")
    val totalPages : Int

) {}