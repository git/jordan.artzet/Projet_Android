package fr.iut.pm.movieapplication.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import fr.iut.pm.movieapplication.databinding.FragmentHomeSectionsBinding
import fr.iut.pm.movieapplication.ui.activity.MainActivity
import fr.iut.pm.movieapplication.ui.adapter.MediaAdapter
import fr.iut.pm.movieapplication.ui.adapter.HomeItemDecoration
import fr.iut.pm.movieapplication.ui.interfaces.MediaSelection
import fr.iut.pm.movieapplication.ui.interfaces.MovieSelection
import fr.iut.pm.movieapplication.ui.viewmodel.HomeSectionsVM

class HomeSectionsFragment() : Fragment(), MediaSelection {

    private val homeSectionsVM by viewModels<HomeSectionsVM>()

    private val trendsAdapter = MediaAdapter(this)
    private val popularMoviesAdapter = MediaAdapter(this)
    private val popularTvShowsAdapter = MediaAdapter(this)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?)
    : View? {

        val binding = FragmentHomeSectionsBinding.inflate(inflater)
        binding.homeSectionsVM = homeSectionsVM
        binding.lifecycleOwner = viewLifecycleOwner

        with(binding.homeTrendsRecyclerView) {
            adapter = trendsAdapter
            addItemDecoration(HomeItemDecoration())
        }

        with(binding.homePopularMoviesRecyclerView) {
            adapter = popularMoviesAdapter
            addItemDecoration(HomeItemDecoration())
        }

        with(binding.homePopularTvShowsRecyclerView) {
            adapter = popularTvShowsAdapter
            addItemDecoration(HomeItemDecoration())
        }


        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        homeSectionsVM.getTrendsLiveData().observe(viewLifecycleOwner) {
            trendsAdapter.submitList(it)
        }

        homeSectionsVM.getPopularMoviesLiveData().observe(viewLifecycleOwner) {
            popularMoviesAdapter.submitList(it)
        }

        homeSectionsVM.getPopularTvShowsLiveData().observe(viewLifecycleOwner) {
            popularTvShowsAdapter.submitList(it)
        }
    }
    override fun onMediaSelected(mediaId: Int) {
        Log.d("ITEM SELECTED", mediaId.toString())
    }
}