package fr.iut.pm.movieapplication.ui.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import coil.load
import fr.iut.pm.movieapplication.R
import fr.iut.pm.movieapplication.databinding.MovieDialogBinding
import fr.iut.pm.movieapplication.ui.viewmodel.MoviesDialogVM
import fr.iut.pm.movieapplication.ui.viewmodel.MoviesDialogVMFactory
import fr.iut.pm.movieapplication.utils.Constants

class MovieDialog : DialogFragment() {

    val moviesDialogVM by viewModels<MoviesDialogVM> { MoviesDialogVMFactory(arguments?.getInt("movieId") ?: 0) }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = MovieDialogBinding.inflate(inflater)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.closeItem.setOnClickListener { dismiss() }
        moviesDialogVM.getMovieDetailLiveData().observe(viewLifecycleOwner) {
            binding.movieDetails = it
            binding.detailsImage.load(it.posterPath?.let { it ->
                (Constants.IMG_URL + it).toUri().buildUpon().scheme("https").build()
            })
        }

        binding.registerMovieDetailsButton.setOnClickListener {
            moviesDialogVM.register(binding.movieDetails!!)
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


    }
}