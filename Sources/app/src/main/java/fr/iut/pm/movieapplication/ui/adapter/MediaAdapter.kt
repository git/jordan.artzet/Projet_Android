package fr.iut.pm.movieapplication.ui.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import fr.iut.pm.movieapplication.databinding.ItemHorizontalHomePageBinding
import fr.iut.pm.movieapplication.model.media.MediaResult
import fr.iut.pm.movieapplication.ui.interfaces.MediaSelection
import fr.iut.pm.movieapplication.ui.interfaces.MovieSelection
import fr.iut.pm.movieapplication.utils.Constants

class MediaAdapter(private val listener : MediaSelection): ListAdapter<MediaResult, MediaAdapter.ViewHolder>(DiffUtilMediaCallback) {

    private object DiffUtilMediaCallback : DiffUtil.ItemCallback<MediaResult>() {
        override fun areItemsTheSame(oldItem: MediaResult, newItem: MediaResult) = oldItem.id == newItem.id
        override fun areContentsTheSame(oldItem: MediaResult, newItem: MediaResult) = oldItem == newItem
    }

    class ViewHolder(private val binding : ItemHorizontalHomePageBinding, listener: MediaSelection)
        : RecyclerView.ViewHolder(binding.root) {

        val mediaResult : MediaResult? get() = binding.mediaResult

        fun bind(mediaResult : MediaResult) {
            binding.mediaResult = mediaResult
            val imgUri = mediaResult.posterPath?.let { (Constants.IMG_URL +it).toUri().buildUpon().scheme("https").build() }
            binding.itemImage.load(imgUri)
            binding.executePendingBindings()
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(ItemHorizontalHomePageBinding.inflate(LayoutInflater.from(parent.context)), listener)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(getItem(position))
}