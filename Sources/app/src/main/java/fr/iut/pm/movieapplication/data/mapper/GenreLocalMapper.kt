package fr.iut.pm.movieapplication.data.mapper

import fr.iut.pm.movieapplication.data.entities.GenreEntity
import fr.iut.pm.movieapplication.model.Genre

object GenreLocalMapper {

    /**
     * Map a [Genre] to a [GenreEntity]
     */
    fun mapToGenreEntity(genre : Genre) : GenreEntity {
        return GenreEntity(
            genreId = genre.id,
            name = genre.name
        )
    }

    /**
     * Map a list of [Genre] to a list of [GenreEntity]
     */
    fun mapToGenreEntities(genres : List<Genre>) : List<GenreEntity> {
        return genres.map { mapToGenreEntity(it) }
    }

    /**
     * Map a [GenreEntity] to a [Genre]
     */
    fun mapToGenre(genreEntity : GenreEntity) : Genre {
        return Genre(
            id = genreEntity.genreId,
            name = genreEntity.name
        )
    }
}