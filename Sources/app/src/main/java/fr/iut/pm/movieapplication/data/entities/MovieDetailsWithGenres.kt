package fr.iut.pm.movieapplication.data.entities

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

data class MovieDetailsWithGenres(
    @Embedded val movieDetails: MovieDetailsEntity,
    @Relation(
        parentColumn = "movie_id",
        entityColumn = "genre_id",
        associateBy = Junction(MovieDetailsGenreEntity::class)
    )
    val genres: List<GenreEntity>
)