package fr.iut.pm.movieapplication.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import fr.iut.pm.movieapplication.R
import fr.iut.pm.movieapplication.databinding.FragmentTvShowsBinding
import fr.iut.pm.movieapplication.ui.activity.MainActivity
import fr.iut.pm.movieapplication.ui.viewmodel.TvShowVM

class TvShowsFragment(
) : Fragment() {


    private val tvShowVM by viewModels<TvShowVM>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?)
    : View {
        val binding = FragmentTvShowsBinding.inflate(inflater)

        binding.tvShowVM = tvShowVM
        binding.lifecycleOwner = viewLifecycleOwner

        val adapter = ArrayAdapter.createFromResource(
            requireContext(),
            R.array.tv_show_filter,
            android.R.layout.simple_spinner_item
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        with(binding.categoryTvShowSpinner)
        {
            this.adapter = adapter
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    tvShowVM.getData(selectedItem.toString())
                }


                override fun onNothingSelected(parent: AdapterView<*>?) {
                    TODO("Not yet implemented")
                }

            }
        }


        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvShowVM.getTvShowLiveData().observe(viewLifecycleOwner) {
            tvShowVM.tvShowAdapter.submitList(it)
        }
    }
}