package fr.iut.pm.movieapplication.model

import androidx.room.Entity

@Entity(tableName = "production_country_table")
data class ProductionCountry(
    var iso_3611_1 : String,
    var name : String
) {

}