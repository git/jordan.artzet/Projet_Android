package fr.iut.pm.movieapplication.api.dtos

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@Json(name = "results")
open class MediaResultDTO(

    @Json(name = "poster_path")
    val posterPath : String?,
    val adult : Boolean?,
    val overview : String? = null,
    @Json(name = "first_air_date")
    val firstAirDate : String? = null,
    @Json(name = "release_date")
    val releaseDate : String? = null,
    @Json(name = "origin_country")
    val originCountry : List<String>? = null,
//    @Json(name = "genre_ids")
//    val genreIds : List<Int>,
    val id : Int,
    @Json(name = "original_title")
    val originalTitle : String? = null,
    @Json(name = "original_language")
    val originalLanguage : String? = null,
    val title : String? = null,
    @Json(name = "backdrop_path")
    val backdropPath : String?,
    val popularity : Double?,
    @Json(name = "vote_count")
    val voteCount : Int?,
    //val video : Boolean?,
    @Json(name = "vote_average")
    val voteAverage : Double?,
    val name: String? = null,
    @Json(name = "original_name")
    val originalName : String? = null,
    @Json(name = "media_type")
    val mediaType : String? = null
) {

}