package fr.iut.pm.movieapplication.data.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey

@Entity(tableName = "movie_details_genre",
    primaryKeys = ["movie_id", "genre_id"],
    foreignKeys = [
        ForeignKey(entity = MovieDetailsEntity::class, parentColumns = ["movie_id"], childColumns = ["movie_id"]),
        ForeignKey(entity = GenreEntity::class, parentColumns = ["genre_id"], childColumns = ["genre_id"])
    ]
)
data class MovieDetailsGenreEntity(
    @ColumnInfo("movie_id") val movieId : Int,
    @ColumnInfo("genre_id") val genreId : Int
)