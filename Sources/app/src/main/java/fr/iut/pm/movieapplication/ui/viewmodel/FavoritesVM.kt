package fr.iut.pm.movieapplication.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fr.iut.pm.movieapplication.data.database.MovieDataBase
import fr.iut.pm.movieapplication.model.media.movie.Movie
import fr.iut.pm.movieapplication.repository.local.MovieLocalRepository
import kotlinx.coroutines.launch

class FavoritesVM : ViewModel() {

    private val repository = MovieLocalRepository(MovieDataBase.getInstance().movieDAO())

    private var _favoritesLiveData : MutableLiveData<List<Movie>> = MutableLiveData()

    fun getFavoritesLiveData() : MutableLiveData<List<Movie>> = _favoritesLiveData


    init {
        //with dispatchers.IO
        viewModelScope.launch{
            _favoritesLiveData.value = repository.getAllMovies()
        }



    }

}