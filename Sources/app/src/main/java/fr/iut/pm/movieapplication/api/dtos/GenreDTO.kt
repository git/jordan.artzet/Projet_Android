package fr.iut.pm.movieapplication.api.dtos

data class GenreDTO(
    val id : Int,
    val name : String
) {

}
