package fr.iut.pm.movieapplication.ui.interfaces

interface MovieSelection {

    fun onMovieSelected(movieId : Int)
}