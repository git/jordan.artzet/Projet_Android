package fr.iut.pm.movieapplication.api.dtos

import com.squareup.moshi.Json
import fr.iut.pm.movieapplication.model.Genre

data class MovieDetailsDTO(

    val adult: Boolean,
    @Json(name = "backdrop_path")
    val backdropPath: String?,
    val budget: Int,
    val genres: List<GenreDTO>,
    val homepage: String?,
    val id: Int,
    @Json(name = "original_language")
    val originalLanguage: String,
    @Json(name = "original_title")
    val originalTitle: String,
    val overview: String?,
    val popularity: Double,
    @Json(name = "poster_path")
    val posterPath: String?,
    //prod companies
    //prod countries
    @Json(name = "release_date")
    val releaseDate: String,
    val revenue: Long,
    //spoken language
    val status: String,
    val title: String,
    @Json(name = "vote_average")
    val voteAverage: Double,
    @Json(name = "vote_count")
    val voteCount: Int

)
{}