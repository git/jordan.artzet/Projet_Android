package fr.iut.pm.movieapplication.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import fr.iut.pm.movieapplication.databinding.FragmentFavoritesCategoryBinding
import fr.iut.pm.movieapplication.ui.adapter.MovieAdapter
import fr.iut.pm.movieapplication.ui.dialog.MovieDialog
import fr.iut.pm.movieapplication.ui.interfaces.MovieSelection
import fr.iut.pm.movieapplication.ui.viewmodel.FavoritesVM

class FavoritesFragment : Fragment(), MovieSelection {

    private val favoritesVM by viewModels<FavoritesVM>()

    private val movieAdapter = MovieAdapter(this);

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentFavoritesCategoryBinding.inflate(inflater)

        binding.favoritesVM = favoritesVM
        binding.lifecycleOwner = viewLifecycleOwner

        with(binding.moviesItemRecyclerView) {
            this.adapter = movieAdapter
        }


        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        favoritesVM.getFavoritesLiveData().observe(viewLifecycleOwner) {
            movieAdapter.submitList(it)
        }
    }





    override fun onMovieSelected(movieId: Int) {
        val dialog = MovieDialog()
        val args = Bundle()
        args.putInt("movieId",movieId)
        dialog.arguments = args
        dialog.show(parentFragmentManager, "tag")
    }
}