package fr.iut.pm.movieapplication.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import fr.iut.pm.movieapplication.databinding.ItemTvShowCategoryBinding
import fr.iut.pm.movieapplication.model.media.tvshow.TvShow
import fr.iut.pm.movieapplication.utils.Constants

class TvShowAdapter() : ListAdapter<TvShow, TvShowAdapter.ViewHolder>(DiffUtilTvShowCallback) {


    private object DiffUtilTvShowCallback : DiffUtil.ItemCallback<TvShow>() {

        override fun areItemsTheSame(oldItem: TvShow, newItem: TvShow) = oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: TvShow, newItem: TvShow) = oldItem == newItem
    }


    class ViewHolder(private val binding : ItemTvShowCategoryBinding)
        : RecyclerView.ViewHolder(binding.root){

        val tvShow : TvShow? get() = binding.tvShow

        init {
            itemView.setOnClickListener {}
        }

        fun bind(tvShow : TvShow) {
            binding.tvShow = tvShow
            val imgUri = tvShow.posterPath?.let { (Constants.IMG_URL +it).toUri().buildUpon().scheme("https").build() }
            binding.itemImage.load(imgUri)
            binding.executePendingBindings()
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TvShowAdapter.ViewHolder =
        TvShowAdapter.ViewHolder(ItemTvShowCategoryBinding.inflate(LayoutInflater.from(parent.context)))

    override fun onBindViewHolder(holder: TvShowAdapter.ViewHolder, position: Int) = holder.bind(getItem(position))

}