package fr.iut.pm.movieapplication.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "production_companies_table")
data class ProductionCompany(
    var name : String,
    @PrimaryKey
    @ColumnInfo(name = "id")
    var productionCompanyId : Int,
    //var logoPath : String?,
    @ColumnInfo(name="origin_country")
    var originCountry : String
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ProductionCompany

        if (productionCompanyId != other.productionCompanyId) return false

        return true
    }

    override fun hashCode(): Int {
        return productionCompanyId
    }
}