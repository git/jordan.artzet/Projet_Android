package fr.iut.pm.movieapplication.data.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "movies_table")
data class MovieEntity(
    @PrimaryKey val id: Int,
    val title: String,
    val overview: String?,
    @ColumnInfo("poster_path") val posterPath: String?,
    @ColumnInfo("backdrop_path") val backdropPath: String?,
    @ColumnInfo("release_date") val releaseDate: String,
    @ColumnInfo("vote_average") val voteAverage: Double,
    @ColumnInfo("vote_count") val voteCount: Int,
    val popularity: Double,
    @ColumnInfo("original_language") val originalLanguage: String,
    @ColumnInfo("original_title") val originalTitle: String,
    val adult: Boolean,
    @ColumnInfo("is_favorite") val isFavorite: Boolean
) {
}