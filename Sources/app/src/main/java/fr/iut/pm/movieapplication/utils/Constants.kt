package fr.iut.pm.movieapplication.utils

import fr.iut.pm.movieapplication.BuildConfig

class Constants {

    companion object {

        //API
        const val BASE_URL = "https://api.themoviedb.org/3/"
        const val IMG_URL = "https://image.tmdb.org/t/p/w500"
        const val API_KEY = BuildConfig.API_KEY


        //VIEW PAGINATION
        const val PAGE_SIZE = 20
        const val MAX_PAGE = 1000
    }
}