package fr.iut.pm.movieapplication.data.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "genres_table")
data class GenreEntity(
    @PrimaryKey @ColumnInfo("genre_id") val genreId : Int,
    val name : String
) {
}