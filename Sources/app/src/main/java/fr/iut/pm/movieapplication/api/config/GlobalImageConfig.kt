package fr.iut.pm.movieapplication.api.config

import android.util.Log
import com.squareup.moshi.Json


object GlobalImageConfig {
    @Json(name = "base_url")
    private var _baseUrl : String = ""
    val baseUrl : String = _baseUrl
    @Json(name = "secure_base_url")
    private var _secureBaseUrl : String = ""
    val secureBaseUrl = _secureBaseUrl
    @Json(name = "backdrop_sizes")
    private var backdropSizes : List<String> = listOf()
    @Json(name = "logo_sizes")
    private var logoSizes : List<String> = listOf()
    @Json(name = "poster_sizes")
    private var posterSizes : List<String> = listOf()
    @Json(name = "profile_sizes")
    private var profilSizes : List<String> = listOf()
    @Json(name = "still_sizes")
    private var stillSizes : List<String> = listOf()

    fun updateConfig(config: ImageConfig) {
        Log.d("BASE URL IMAGE", baseUrl)
        _baseUrl = config.baseUrl
        _secureBaseUrl = config.secureBaseUrl
        posterSizes = config.posterSizes
        backdropSizes = config.backdropSizes
    }

}

data class ImageConfig(

    @Json(name = "images.base_url")
    val baseUrl : String,
    @Json(name = "images.secure_base_url")
    val secureBaseUrl : String,
    @Json(name = "images.backdrop_sizes")
    val backdropSizes : List<String>,
    @Json(name = "images.logo_sizes")
    val logoSizes : List<String>,
    @Json(name = "images.poster_sizes")
    val posterSizes : List<String>,
    @Json(name = "images.profile_sizes")
    val profileSizes : List<String>,
    @Json(name = "images.still_sizes")
    val stillSizes : List<String>
)