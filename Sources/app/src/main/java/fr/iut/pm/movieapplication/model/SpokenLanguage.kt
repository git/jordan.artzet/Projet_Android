package fr.iut.pm.movieapplication.model

data class SpokenLanguage(
    var iso_639_1 : String,
    var name : String
) {

}
