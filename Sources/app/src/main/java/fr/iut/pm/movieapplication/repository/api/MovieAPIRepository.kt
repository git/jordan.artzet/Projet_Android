package fr.iut.pm.movieapplication.repository.api

import android.util.Log
import fr.iut.pm.movieapplication.api.RetrofitInstance
import fr.iut.pm.movieapplication.model.media.movie.Movie
import fr.iut.pm.movieapplication.model.media.movie.MovieDetails
import fr.iut.pm.movieapplication.utils.MediaResultMapper
import fr.iut.pm.movieapplication.utils.MovieMapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MovieAPIRepository {
    suspend fun getPopularMovies(page : Int = 1) : List<Movie> = withContext(Dispatchers.IO)
    {

        val listMovie : MutableList<Movie> = mutableListOf()

        val response = RetrofitInstance.api.getPopularMovies(page = page)
        if(response.isSuccessful) {
            val listMediaResultDTO = response.body()?.results
            listMediaResultDTO?.forEach {
                val movie = MediaResultMapper.mapToMovie(it)
                listMovie.add(movie)
                Log.d("Movie ", movie.title!! + " " + movie.id)
            }
        }
        else Log.d("ERROR FAILED", response.message())
        listMovie
    }

    suspend fun getNowPlayingMovies(page : Int = 1) : List<Movie> = withContext(Dispatchers.IO)
    {

        val listMovie : MutableList<Movie> = mutableListOf()

        val response = RetrofitInstance.api.getNowPlayingMovies(page = page)
        if(response.isSuccessful) {
            val listMediaResultDTO = response.body()?.results
            listMediaResultDTO?.forEach {
                val movie = MediaResultMapper.mapToMovie(it)
                listMovie.add(movie)
                Log.d("Movie ", movie.title!!)
            }
        }
        else Log.d("ERROR FAILED", response.message())
        listMovie
    }

    suspend fun getUpcomingMovies(page : Int = 1)  : List<Movie> = withContext(Dispatchers.IO)
    {

        val listMovie : MutableList<Movie> = mutableListOf()

        val response = RetrofitInstance.api.getUpcomingMovies(page = page)
        if(response.isSuccessful) {
            val listMediaResultDTO = response.body()?.results
            listMediaResultDTO?.forEach {
                val movie = MediaResultMapper.mapToMovie(it)
                listMovie.add(movie)
                Log.d("Movie ", movie.title!!)
            }
        }
        else Log.d("ERROR FAILED", response.message())
        listMovie
    }

    suspend fun getTopRatedMovies(page : Int = 1)  : List<Movie> = withContext(Dispatchers.IO)
    {

        val listMovie : MutableList<Movie> = mutableListOf()

        val response = RetrofitInstance.api.getTopRatedMovies(page = page)
        if(response.isSuccessful) {
            val listMediaResultDTO = response.body()?.results
            listMediaResultDTO?.forEach {
                val movie = MediaResultMapper.mapToMovie(it)
                listMovie.add(movie)
                Log.d("Movie ", movie.title!!)
            }
        }
        else Log.d("ERROR FAILED", response.message())
        listMovie
    }

    suspend fun getMovieDetails(id : Int) : MovieDetails?
    {
        var movieDetails : MovieDetails? = null

        val response = RetrofitInstance.api.getMovieDetails(id)
        if(response.isSuccessful && response.body() != null) {

            Log.d("SUCCESS", response.body().toString())
            movieDetails = MovieMapper.mapToMovieDetails(response.body()!!)
            Log.d("Movie details",movieDetails.toString())
        }
        else Log.d("ERROR FAILED", response.toString())



        return movieDetails
    }


}