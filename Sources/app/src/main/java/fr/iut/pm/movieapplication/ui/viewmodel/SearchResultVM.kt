package fr.iut.pm.movieapplication.ui.viewmodel

import androidx.lifecycle.*
import fr.iut.pm.movieapplication.model.media.MediaResult
import fr.iut.pm.movieapplication.repository.api.MediaRepository
import kotlinx.coroutines.launch

class SearchResultVM(private var query : String) : ViewModel() {

    private val repository : MediaRepository = MediaRepository()

    private var _queryLiveData : MutableLiveData<String> = MutableLiveData()
    fun getQueryLiveData() : LiveData<String> = _queryLiveData

    private var _searchResultLiveData : MutableLiveData<List<MediaResult>> = MutableLiveData()
    fun getSearchResultLiveData() : LiveData<List<MediaResult>> = _searchResultLiveData

    init {
        _queryLiveData.value = query
        viewModelScope.launch {
            _searchResultLiveData.postValue(repository.search(query))
        }
    }
}

class SearchResultVMFactory(private var query : String) : ViewModelProvider.Factory
{
    override fun<T:ViewModel>create(modelClass:Class<T>) : T
    {
        return SearchResultVM(query) as T
    }
}