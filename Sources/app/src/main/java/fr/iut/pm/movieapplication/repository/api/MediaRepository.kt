package fr.iut.pm.movieapplication.repository.api

import android.util.Log
import fr.iut.pm.movieapplication.api.RetrofitInstance
import fr.iut.pm.movieapplication.model.media.MediaResult
import fr.iut.pm.movieapplication.utils.MediaResultMapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MediaRepository {

    suspend fun getTrends() : List<MediaResult> = withContext(Dispatchers.IO) {
        val listMediaResult : MutableList<MediaResult> = mutableListOf()

        val response = RetrofitInstance.api.getTrending()
        if(response.isSuccessful) {

                val listMediaResultDTO = response.body()?.results
                Log.d("Response",response.body().toString())
                    listMediaResultDTO?.forEach {

                        val mediaResult = MediaResultMapper.mapToMediaResult(it)
                        listMediaResult.add(mediaResult)
                        mediaResult.title?.let { it1 -> Log.d("Movie", it1) }

                    }
        }
        else Log.d("ERROR FAILED", response.message())
        listMediaResult
    }

    suspend fun getPopularMovies() : List<MediaResult> = withContext(Dispatchers.IO) {
        val listMediaResult : MutableList<MediaResult> = mutableListOf()

        val response = RetrofitInstance.api.getPopularMovies()
        if(response.isSuccessful) {

            val listMediaResultDTO = response.body()?.results
            Log.d("Response",response.body().toString())
            listMediaResultDTO?.forEach {

                val mediaResult = MediaResultMapper.mapToMediaResult(it)
                listMediaResult.add(mediaResult)
                mediaResult.title?.let { it1 -> Log.d("Movie", it1) }

            }
        }
        else Log.d("ERROR FAILED", response.message())
        listMediaResult
    }

    suspend fun getPopularTvShows(): List<MediaResult> = withContext(Dispatchers.IO) {
        val listMediaResult : MutableList<MediaResult> = mutableListOf()

        val response = RetrofitInstance.api.getPopularTvShows()
        if(response.isSuccessful) {

            val listMediaResultDTO = response.body()?.results
            Log.d("Response",response.body().toString())
            listMediaResultDTO?.forEach {

                val mediaResult = MediaResultMapper.mapToMediaResult(it)
                listMediaResult.add(mediaResult)
                mediaResult.title?.let { it1 -> Log.d("Movie", it1) }

            }
        }
        else Log.d("ERROR FAILED", response.message())
        listMediaResult
    }

    suspend fun search(query : String, Page : Int = 1) : List<MediaResult> = withContext(Dispatchers.IO) {
        val listMediaResult : MutableList<MediaResult> = mutableListOf()

        val response = RetrofitInstance.api.searchMedia(query, Page)
        if(response.isSuccessful) {

            val listMediaResultDTO = response.body()?.results
            Log.d("Response",response.body().toString())
            listMediaResultDTO?.forEach {
                if(it?.mediaType == "movie" || it?.mediaType == "tv") {
                    val mediaResult = it?.let { it1 -> MediaResultMapper.mapToMediaResult(it1) }
                    mediaResult?.let { it1 -> listMediaResult.add(it1) }
                    mediaResult?.let { it1 -> Log.d("Movie", it1.title) }
                }

            }
        }
        else Log.d("ERROR FAILED", response.message())
        listMediaResult
    }
}