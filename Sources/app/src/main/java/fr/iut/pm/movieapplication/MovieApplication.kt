package fr.iut.pm.movieapplication

import android.app.Application
import androidx.room.Room
import fr.iut.pm.movieapplication.data.database.MovieDataBase

class MovieApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        MovieDataBase.initialize(this)
    }
}