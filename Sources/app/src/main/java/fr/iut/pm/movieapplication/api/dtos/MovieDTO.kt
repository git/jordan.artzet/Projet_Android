package fr.iut.pm.movieapplication.api.dtos

import com.squareup.moshi.Json

open class MovieDTO (
    @Json(name = "poster_path")
    open val posterPath: String?,
    open val adult: Boolean,
    open val overview: String?,
    @Json(name = "release_date")
    open val releaseDate: String,
    open val id: Int,
    @Json(name = "original_title")
    open val originalTitle: String,
    @Json(name = "original_language")
    open val originalLanguage: String,
    open val title: String,
    @Json(name = "backdrop_path")
    open val backdropPath: String?,
    open val popularity: Double,
    @Json(name = "vote_count")
    open val voteCount: Int,
    @Json(name = "vote_average")
    open val voteAverage: Double


){

}