package fr.iut.pm.movieapplication.ui.viewmodel

import androidx.lifecycle.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.iut.pm.movieapplication.model.media.movie.Movie
import fr.iut.pm.movieapplication.repository.api.MovieAPIRepository
import fr.iut.pm.movieapplication.utils.Constants.Companion.MAX_PAGE
import kotlinx.coroutines.launch

class MoviesVM() : ViewModel() {
    /**
     * The movie repository used to get our data
     */
    private val repository = MovieAPIRepository()

    /**
     * The MutableLiveData
     */
    private var _moviesLiveData : MutableLiveData<List<Movie>> = MutableLiveData<List<Movie>>()

    /**
     * Getter of the LiveData
     */
    fun getMoviesLiveData() : LiveData<List<Movie>> = _moviesLiveData

    /**
     * The current data filter
     */
    private var currentFilter = ""

    /**
     * The adapter of the RecyclerView (set on the RecyclerView in the view)
     */


    val scrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            val layoutManager = recyclerView.layoutManager as GridLayoutManager
            val lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition()
            val totalItemCount = layoutManager.itemCount

            if(lastVisibleItemPosition == totalItemCount -1) {

                ++currentPage
                //1000 is the MAX_PAGE
                if(currentPage <= MAX_PAGE) getMoreData(currentPage)
            }
        }
    }

    /**
     * Currrent page where the data are obtained
     */
    private var currentPage = 1

    /**
     * Get the data with a given filter
     * @param filter filter applied to get data
     */
    fun getData(filter : String) {
        currentFilter = filter
        currentPage = 1
        when(currentFilter) {
            "Populaires" -> viewModelScope.launch {
                _moviesLiveData.postValue(repository.getPopularMovies())
                repository.getMovieDetails(315162)
            }
            "Du moment" -> viewModelScope.launch {
                _moviesLiveData.postValue(repository.getNowPlayingMovies())
            }
            "À venir" -> viewModelScope.launch {
                _moviesLiveData.postValue(repository.getUpcomingMovies())
            }
            "Les mieux notés" -> viewModelScope.launch {
                _moviesLiveData.postValue(repository.getTopRatedMovies())
            }
        }

    }

    /**
     * Get more data with the actual filter
     * @param page page from which the data are obtained
     */
    private fun getMoreData(page : Int = 1) {
        var movies : List<Movie>
        when(currentFilter) {
            "Populaires" -> viewModelScope.launch {
                movies = _moviesLiveData.value?.plus(repository.getPopularMovies(page)) ?: listOf()
                _moviesLiveData.postValue(movies)
            }
            "Du moment" -> viewModelScope.launch {
                movies = _moviesLiveData.value?.plus(repository.getNowPlayingMovies(page)) ?: listOf()
                _moviesLiveData.postValue(movies)
            }
            "À venir" -> viewModelScope.launch {
                movies = _moviesLiveData.value?.plus(repository.getUpcomingMovies(page)) ?: listOf()
                _moviesLiveData.postValue(movies)
            }
            "Les mieux notés" -> viewModelScope.launch {
                movies = _moviesLiveData.value?.plus(repository.getTopRatedMovies(page)) ?: listOf()
                _moviesLiveData.postValue(movies)
            }
        }

    }

}