package fr.iut.pm.movieapplication.data.mapper

import fr.iut.pm.movieapplication.data.entities.MovieDetailsEntity
import fr.iut.pm.movieapplication.data.entities.MovieDetailsWithGenres
import fr.iut.pm.movieapplication.data.entities.MovieEntity
import fr.iut.pm.movieapplication.model.media.movie.Movie
import fr.iut.pm.movieapplication.model.media.movie.MovieDetails

object MovieLocalMapper {

    /**
     * Map a [MovieEntity] to a [Movie]
     */
    fun mapToMovie(movieEntity : MovieEntity) : Movie {
        return Movie(
            posterPath = movieEntity.posterPath,
            adult = movieEntity.adult,
            overview = movieEntity.overview,
            releaseDate = movieEntity.releaseDate,
            id = movieEntity.id,
            originalTitle = movieEntity.originalTitle,
            originalLanguage = movieEntity.originalLanguage,
            title = movieEntity.title,
            backdropPath = movieEntity.backdropPath,
            popularity = movieEntity.popularity,
            voteCount = movieEntity.voteCount,
            voteAverage = movieEntity.voteAverage
        )
    }


    /**
     * Map a [Movie] to a [MovieEntity]
     */
    fun mapToMovieEntity(movie : Movie) : MovieEntity {
        return MovieEntity(
            posterPath = movie.posterPath,
            adult = movie.adult!!,
            overview = movie.overview,
            releaseDate = movie.releaseDate!!,
            id = movie.id,
            originalTitle = movie.originalTitle,
            originalLanguage = movie.originalLanguage,
            title = movie.title,
            backdropPath = movie.backdropPath ,
            popularity = movie.popularity,
            voteCount = movie.voteCount,
            voteAverage = movie.voteAverage,
            isFavorite = false
        )
    }

    /**
     * Map a [MovieDetails] to a [MovieDetailsEntity]
     */
    fun mapToMovieDetailsEntity(movieDetails : MovieDetails) : MovieDetailsEntity {
        return MovieDetailsEntity(
            movieId = movieDetails.id,
            movie = mapToMovieEntity(movieDetails),
            budget = movieDetails.budget,
            homepage = movieDetails.homepage,
            revenue = movieDetails.revenue,
            status = movieDetails.status
        )
    }

    /**
     * Map a [MovieDetails] to a [MovieDetailsWithGenres]
     */
    fun mapToMovieDetailsWithGenres(movieDetails: MovieDetails) : MovieDetailsWithGenres {
        return MovieDetailsWithGenres(
            movieDetails = mapToMovieDetailsEntity(movieDetails),
            genres = movieDetails.genres.map { GenreLocalMapper.mapToGenreEntity(it) }
        )
    }

    /**
     * Map a [MovieDetailsWithGenres] to a [MovieDetails]
     */
    fun mapToMovieDetails(movieDetailsWithGenres: MovieDetailsWithGenres) : MovieDetails {
        return MovieDetails(
            id = movieDetailsWithGenres.movieDetails.movieId,
            posterPath = movieDetailsWithGenres.movieDetails.movie.posterPath,
            adult = movieDetailsWithGenres.movieDetails.movie.adult,
            overview = movieDetailsWithGenres.movieDetails.movie.overview,
            releaseDate = movieDetailsWithGenres.movieDetails.movie.releaseDate,
            originalTitle = movieDetailsWithGenres.movieDetails.movie.originalTitle,
            originalLanguage = movieDetailsWithGenres.movieDetails.movie.originalLanguage,
            title = movieDetailsWithGenres.movieDetails.movie.title,
            backdropPath = movieDetailsWithGenres.movieDetails.movie.backdropPath,
            popularity = movieDetailsWithGenres.movieDetails.movie.popularity,
            voteCount = movieDetailsWithGenres.movieDetails.movie.voteCount,
            voteAverage = movieDetailsWithGenres.movieDetails.movie.voteAverage,
            budget = movieDetailsWithGenres.movieDetails.budget,
            homepage = movieDetailsWithGenres.movieDetails.homepage,
            revenue = movieDetailsWithGenres.movieDetails.revenue,
            status = movieDetailsWithGenres.movieDetails.status,
            genres = movieDetailsWithGenres.genres.map { GenreLocalMapper.mapToGenre(it) }
        )
    }
}