package fr.iut.pm.movieapplication.model.media.movie

import fr.iut.pm.movieapplication.model.Genre


class MovieDetails(

    adult : Boolean?,
    backdropPath : String?,
    val budget : Int,
    val genres : List<Genre>,
    val homepage : String?,
    id : Int,
    originalLanguage : String,
    originalTitle : String,
    overview : String?,
    popularity : Double,
    posterPath : String?,
    //prod companies
    //prod countries
    releaseDate : String?,
    val revenue : Long,
    //spoken language
    val status : String,
    title : String,
    voteAverage : Double,
    voteCount : Int

) : Movie(posterPath, adult, overview, releaseDate, id, originalTitle, originalLanguage,
    title, backdropPath, popularity, voteCount, voteAverage)
{


}