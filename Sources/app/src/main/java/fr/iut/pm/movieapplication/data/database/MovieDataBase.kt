package fr.iut.pm.movieapplication.data.database

import android.app.Application
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import fr.iut.pm.movieapplication.MovieApplication
import fr.iut.pm.movieapplication.data.dao.MovieDAO
import fr.iut.pm.movieapplication.data.entities.GenreEntity
import fr.iut.pm.movieapplication.data.entities.MovieDetailsEntity
import fr.iut.pm.movieapplication.data.entities.MovieDetailsGenreEntity
import fr.iut.pm.movieapplication.data.entities.MovieEntity


const val MOVIE_DB_NAME = "movies.db"

@Database( entities = [MovieEntity::class, MovieDetailsEntity::class, GenreEntity::class, MovieDetailsGenreEntity::class], version = 3)
abstract class MovieDataBase : RoomDatabase()
{
    abstract fun movieDAO() : MovieDAO

    companion object
    {
        private lateinit var application : Application

        @Volatile
        private var instance : MovieDataBase? = null

        fun getInstance() : MovieDataBase
        {
            if (::application.isInitialized) {
                if (instance == null)
                    synchronized(this)
                    {
                        if (instance == null)
                            instance = Room.databaseBuilder(
                                application.applicationContext,
                                MovieDataBase::class.java,
                                MOVIE_DB_NAME
                            )
                                .fallbackToDestructiveMigration()
                                .build()
                    }
                return instance!!
            }
            else
                throw RuntimeException("the database must be first initialized")
        }

        @Synchronized
        fun initialize(app : MovieApplication)
        {
            if (::application.isInitialized)
                throw RuntimeException("the database must not be initialized twice")

            application = app
        }
    }
}

