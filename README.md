# DB Movie Application

## Overview
___
This project use the API of [The Movie Database](https://www.themoviedb.org/?language=fr)


This project is still in development :construction:

## Documentation
___


## Getting started
___


### Prerequisites

Here are the prerequisites before launching the project.
|  |  |
| ---- | ---- |
| The integrated Development Environment (IDE) Android Studios |<img  src="Documentation/images/Android_Studio_Trademark.png" alt="drawing" width="95" height="30" />  |

### Dependencies

- AndroidX
- Retrofit2 to use the api requests and moshi to parse data
- Coil for load the images
- Room Database to store local data 
- The databinding
- JUnit is included too because i want to add tests on the project in the future


### Installation

To launch the project, open `Android Studios` and click on `file` on the left corner.
Click on `open` and open the `Sources` folder in the project and let the IDE index the files